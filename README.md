Automatic Installation

Run the following instructions in a Notebook of Mathematica

Import["https://gitlab.com/ed_in/mathspaceapp/raw/master/installscript2.m"]

InstallSpaceMath


___________________________________________________________________________________

Manual installation

Download (https://github.com/elecu/install_test/raw/master/APPStestI.zip) APPStestI
Unizip the folder inside $UserBaseDirectory/Applications


This should install SpaceMathApp on your system.